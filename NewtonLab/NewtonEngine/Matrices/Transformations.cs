﻿using System;

namespace NewtonEngine.Matrices
{
    /// <summary>
    /// This class contains methods to create 4x4 matrices representing 3D transformations using
    /// homogeneous coordinates.
    /// </summary>
    public static class Transformations
    {
        // The matrice's transformation matrices' dimensions.
        private const int SIZE = 4;

        #region Identity

        /// <summary>
        /// Creates the identity matrix.
        /// </summary>
        /// <returns>The identity matrix.</returns>
        public static Matrix Identity()
        {
            Matrix m = new Matrix(SIZE);

            for (int i = 0; i < SIZE; ++i)
            {
                m[i, i] = 1.0;
            }

            return m;
        }

        #endregion

        #region Translation

        /// <summary>
        /// Creates the transformation matrix for a translation.
        /// </summary>
        /// <param name="v">Translation vector.</param>
        /// <returns>The matrix for a translation by v.</returns>
        public static Matrix Translation(Vector v)
        {
            Matrix m = Identity();

            m[3, 0] = v.X;
            m[3, 1] = v.Y;
            m[3, 2] = v.Z;

            return m;
        }

        /// <summary>
        /// Creates the transformation matrix for a translation.
        /// </summary>
        /// <param name="dx">Translation amount in x-direction.</param>
        /// <param name="dy">Translation amount in y-direction.</param>
        /// <param name="dz">Translation amount in z-direction.</param>
        /// <returns>The matrix for a translation by (dx, dy, dz).</returns>
        public static Matrix Translation(double dx, double dy, double dz)
        {
            Matrix m = Identity();

            m[3, 0] = dx;
            m[3, 1] = dy;
            m[3, 2] = dz;

            return m;
        }

        #endregion

        #region Scaling

        /// <summary>
        /// Creates the transformation matrix for a scaling.
        /// </summary>
        /// <param name="s">The vector of scaling factors.</param>
        /// <returns>The matrix for a scaling by (s.X, s.Y, s.Z).</returns>
        public static Matrix Scaling(Vector s)
        {
            Matrix m = Identity();

            m[0, 0] = s.X;
            m[1, 1] = s.Y;
            m[2, 2] = s.Z;

            return m;
        }

        /// <summary>
        /// Creates the transformation matrix for a scaling.
        /// </summary>
        /// <param name="sx">Scaling factor in x-direction.</param>
        /// <param name="sy">Scaling factor in y-direction.</param>
        /// <param name="sz">Scaling factor in z-direction.</param>
        /// <returns>The matrix for a scaling by (sx, sy, sz).</returns>
        public static Matrix Scaling(double sx, double sy, double sz)
        {
            Matrix m = Identity();

            m[0, 0] = sx;
            m[1, 1] = sy;
            m[2, 2] = sz;

            return m;
        }
        
        /// <summary>
        /// Creates the transformation matrix for a uniform scaling.
        /// </summary>
        /// <param name="s">Scaling factor in all directions.</param>
        /// <returns>The matrix for a uniform scaling by s.</returns>
        public static Matrix Scaling(double s)
        {
            return Scaling(s, s, s);
        }

        #endregion

        #region Reflection

        /// <summary>
        /// Creates the transformation matrix for a reflection in the yz-plane.
        /// </summary>
        /// <returns>The matrix for a reflection in the yz-plane.</returns>
        public static Matrix ReflectionInYZ()
        {
            return Scaling(-1.0, 1.0, 1.0);
        }

        /// <summary>
        /// Creates the transformation matrix for a reflection in the xz-plane.
        /// </summary>
        /// <returns>The matrix for a reflection in the xz-plane.</returns>
        public static Matrix ReflectionInXZ()
        {
            return Scaling(1.0, -1.0, 1.0);
        }

        /// <summary>
        /// Creates the transformation matrix for a reflection in the xy-plane.
        /// </summary>
        /// <returns>The matrix for a reflection in the xy-plane.</returns>
        public static Matrix ReflectionInXY()
        {
            return Scaling(1.0, 1.0, -1.0);
        }

        #endregion

        #region Rotation

        /// <summary>
        /// Creates the transformation matrix for a counterclockwise rotation about the x-axis.
        /// </summary>
        /// <param name="theta">Rotation angle in radians.</param>
        /// <returns>The matrix for a counterclockwise rotation by theta about the x-axis.</returns>
        public static Matrix RotationAboutX(double theta)
        {
            Matrix m = Identity();

            m[1, 1] =  Math.Cos(theta);
            m[1, 2] =  Math.Sin(theta);
            m[2, 1] = -Math.Sin(theta);
            m[2, 2] =  Math.Cos(theta);

            return m;
        }

        /// <summary>
        /// Creates the transformation matrix for a counterclockwise rotation about the y-axis.
        /// </summary>
        /// <param name="theta">Rotation angle in radians.</param>
        /// <returns>The matrix for a counterclockwise rotation by theta about the y-axis.</returns>
        public static Matrix RotationAboutY(double theta)
        {
            Matrix m = Identity();

            m[0, 0] =  Math.Cos(theta);
            m[0, 2] = -Math.Sin(theta);
            m[2, 0] =  Math.Sin(theta);
            m[2, 2] =  Math.Cos(theta);

            return m;
        }

        /// <summary>
        /// Creates the transformation matrix for a counterclockwise rotation about the z-axis.
        /// </summary>
        /// <param name="theta">Rotation angle in radians.</param>
        /// <returns>The matrix for a counterclockwise rotation by theta about the z-axis.</returns>
        public static Matrix RotationAboutZ(double theta)
        {
            Matrix m = Identity();

            m[0, 0] =  Math.Cos(theta);
            m[0, 1] =  Math.Sin(theta);
            m[1, 0] = -Math.Sin(theta);
            m[1, 1] =  Math.Cos(theta);

            return m;
        }

        /// <summary>
        /// Creates the transformation matrix for a rotation about all three axes.
        /// </summary>
        /// <param name="r">The vector of rotation angles in radians.</param>
        /// <returns>The matrix for a rotation by (r.X, r.Y, r.Z) about the axes.</returns>
        public static Matrix Rotation(Vector r)
        {
            return RotationAboutX(r.X) * RotationAboutY(r.Y) * RotationAboutZ(r.Z);
        }

        /// <summary>
        /// Creates the transformation matrix for a rotation about all three axes.
        /// </summary>
        /// <param name="rx">Rotation angle about x-axis in radians.</param>
        /// <param name="ry">Rotation angle about y-axis in radians.</param>
        /// <param name="rz">Rotation angle about z-axis in radians.</param>
        /// <returns>The matrix for a rotation by (rx, ry, rz) about the axes.</returns>
        public static Matrix Rotation(double rx, double ry, double rz)
        {
            return RotationAboutX(rx) * RotationAboutY(ry) * RotationAboutZ(rz);
        }

        #endregion
    }
}