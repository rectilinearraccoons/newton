﻿using NewtonEngine.Algorithms;
using NewtonEngine.Matrices;
using System;

namespace NewtonEngine
{
    /// <summary>
    /// This is the common parent class for all collidable objects, i.e. objects that are taken
    /// into account for collision detection and collision response.
    /// </summary>
    public abstract class Collidable : PhysicsObject
    {
        #region Fields

        /// <summary>
        /// The default density of a collidable object.
        /// </summary>
        public const double DEFAULT_DENSITY = 1000.0;

        #endregion

        #region Constructors

        /// <summary>
        /// Creates a new collidable object at position (x, y, z).
        /// </summary>
        /// <param name="x">The x-coordinate of the object's position.</param>
        /// <param name="y">The y-coordinate of the object's position.</param>
        /// <param name="z">The z-coordinate of the object's position.</param>
        protected Collidable(double x, double y, double z) : base(x, y, z)
        {
            CollisionEnabled = true;
            Density = DEFAULT_DENSITY;
            RollingSurfaceNormal = Vector.NULL;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Whether collisions are currently enabled for this object.
        /// </summary>
        public bool CollisionEnabled { get; set; }

        /// <summary>
        /// The radius of the object's bounding sphere.
        /// </summary>
        public abstract double Radius { get; }

        /// <summary>
        /// The object's density in kg/m^3.
        /// </summary>
        public double Density 
        {
            get { return Mass / GetVolume();  }
            set { Mass = value * GetVolume(); }
        }

        /// <summary>
        /// The normal of the surface the object is currently touching.
        /// </summary>
        public Vector RollingSurfaceNormal { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Calculates and returns the matrix of vertices of the object's bounding box.
        /// </summary>
        /// <returns>The vertices of the object's bounding box.</returns>
        public abstract Matrix GetBoundingBox();

        /// <summary>
        /// Calculates and returns the object's volume in m^3.
        /// </summary>
        /// <returns>The object's volume in m^3.</returns>
        public abstract double GetVolume();

        /// <summary>
        /// Calculates and returns the normalized normal vector of the object's surface towards the
        /// given position.
        /// </summary>
        /// <param name="position">The position to get the normal vector to.</param>
        /// <returns>The normalized normal vector of the surface towards the position.</returns>
        public abstract Vector GetNormalTo(Vector position);

        /// <summary>
        /// Checks whether the two given objects collide. If they do, rewind them to the correct
        /// positions and resolve the collision by transferring energy (linear momentum).
        /// </summary>
        /// <param name="obj1">The 1st object to check.</param>
        /// <param name="obj2">The 2nd object to check.</param>
        public static void Check(Collidable obj1, Collidable obj2)
        {
            if (obj1.Fixed && obj2.Fixed)
            {
                return;
            }

            double t = 0.0;

            if (obj1.CollisionEnabled &&
                obj2.CollisionEnabled &&
                Collisions.Intersect(obj1, obj2, out t))
            {
                obj1.Rewind(t);
                obj2.Rewind(t);

                Collisions.Resolve(obj1, obj2);

                // Set surface normals if objects keep touching.
                Vector n1 = obj1.GetNormalTo(obj2.Position);
                Vector n2 = obj2.GetNormalTo(obj1.Position);

                if (!obj1.isFixed &&
                    obj1.Velocity.GetLengthSquare() > PhysicsEngine.MIN_VELOCITY_THRESHOLD &&
                    Vector.DotProduct(obj1.Velocity, n2) < 0.2)
                {
                    obj1.RollingSurfaceNormal = n2;
                }

                if (!obj2.isFixed &&
                    obj2.Velocity.GetLengthSquare() > PhysicsEngine.MIN_VELOCITY_THRESHOLD &&
                    Vector.DotProduct(obj2.Velocity, n1) < 0.2)
                {
                    obj2.RollingSurfaceNormal = n1;
                }
            }
        }

        /// <summary>
        /// Rewinds the object by the given amount of time, i.e. resets the object's position and
        /// velocity to what it was (approximately) t seconds ago.
        /// </summary>
        /// <param name="t">The amount of time (in s) to rewind.</param>
        private void Rewind(double t)
        {
            if (Fixed || Velocity.IsNull())
            {
                return;
            }

            Position -= Velocity * t;

            Vector n = Acceleration.Normalize();
            double v = Vector.DotProduct(Velocity, n);

            if (v < PhysicsEngine.MIN_VELOCITY_THRESHOLD)
            {
                return;
            }

            double discriminant = 1 - 2 * Acceleration.GetLength() * t / v;

            if (discriminant < PhysicsEngine.MIN_VELOCITY_THRESHOLD)
            {
                discriminant = 0.0;
            }

            Velocity += (Math.Sqrt(discriminant) - 1) * v * n;
        }

        /// <summary>
        /// Scales the object by (sx, sy, sz). This also adjusts the object's current mass such
        /// that its current density is maintained.
        /// </summary>
        /// <param name="sx">Scaling factor in x-direction.</param>
        /// <param name="sy">Scaling factor in y-direction.</param>
        /// <param name="sz">Scaling factor in z-direction.</param>
        public override void Scale(double sx, double sy, double sz)
        {
            double density = Density;
            base.Scale(sx, sy, sz);
            Density = density;

        }

        /// <summary>
        /// Sets the object's scaling to (sx, sy, sz). This also adjusts the object's current mass
        /// such that its current density is maintained.
        /// </summary>
        /// <param name="sx">Scaling factor in x-direction.</param>
        /// <param name="sy">Scaling factor in y-direction.</param>
        /// <param name="sz">Scaling factor in z-direction.</param>
        public override void SetScaling(double sx, double sy, double sz)
        {
            double density = Density;
            base.SetScaling(sx, sy, sz);
            Density = density;
        }

        #endregion
    }
}