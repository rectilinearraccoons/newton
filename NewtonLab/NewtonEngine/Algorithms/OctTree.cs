﻿using System;
using System.Collections.Generic;

namespace NewtonEngine.Algorithms
{
    /// <summary>
    /// Implementation of an OctTree for collision detection.
    /// </summary>
    internal class OctTree  
    {
        #region Fields

        private OctTreeNode      rootNode;
        private List<Collidable> objects;
        private int              maxRange;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructor for a new OctTree.
        /// </summary>
        /// <param name="objects">The list of all collidable objects.</param>
        /// <param name="maxRange">The maximum range in which objects are considered. If this is
        /// not a power of 2, the next highest power of 2 will be chosen instead.</param>
        public OctTree(List<Collidable> objects, int maxRange)
        {
            this.objects  = objects;
            this.maxRange = (int) Math.Pow(2, Math.Ceiling(Math.Log(maxRange, 2)));
        }

        #endregion

        #region Methods

        /// <summary>
        /// Builds the OctTree around the given centre point.
        /// </summary>
        /// <param name="centreX">The x-coordinate of the root octant's centre.</param>
        /// <param name="centreY">The y-coordinate of the root octant's centre.</param>
        /// <param name="centreZ">The z-coordinate of the root octant's centre.</param>
        public void Build(double centreX, double centreY, double centreZ)
        {
            rootNode = new OctTreeNode(new List<Collidable>(objects),
                null, maxRange, centreX, centreY, centreZ);
        }

        /// <summary>
        /// Performs collision detection on this OctTree.
        /// </summary>
        public void CheckCollisions()
        {
            rootNode.CheckCollision(new List<Collidable>());
        }

        #endregion
    }
}