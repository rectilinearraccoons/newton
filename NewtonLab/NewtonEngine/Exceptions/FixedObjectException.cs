﻿using System;

namespace NewtonEngine.Exceptions
{
    /// <summary>
    /// This exception is thrown when an invalid operation is performed on an object that is fixed,
    /// such as setting the object's velocity or acceleration.
    /// </summary>
    public class FixedObjectException : InvalidOperationException
    {
        private const string ERROR_MESSAGE = "Operation '{0}' is not allowed on fixed objects.";

        /// <summary>
        /// The object that triggered the exception.
        /// </summary>
        public PhysicsObject Object { get; private set; }
        
        /// <summary>
        /// Creates a new excption for the specified object and operation.
        /// </summary>
        /// <param name="obj">The object that triggered the exception.</param>
        /// <param name="operation">A description of the invalid operation.</param>
        public FixedObjectException(PhysicsObject obj, string operation)
            : base(String.Format(ERROR_MESSAGE, operation))
        {
            Object = obj;
        }
    }
}