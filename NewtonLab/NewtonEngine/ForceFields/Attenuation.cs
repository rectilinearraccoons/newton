﻿namespace NewtonEngine.ForceFields
{
    /// <summary>
    /// Different types of attenuation of a force with respect to the distance from its source.
    /// </summary>
    public enum Attenuation
    {
        /// <summary>
        /// No attenuation.
        /// </summary>
        NONE,

        /// <summary>
        /// Linear attenuation.
        /// </summary>
        LINEAR,

        /// <summary>
        /// Quadratic attenuation.
        /// </summary>
        QUADRATIC
    }
}