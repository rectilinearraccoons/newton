﻿using NewtonEngine.Matrices;

namespace NewtonEngine.ForceFields
{
    /// <summary>
    /// This force field causes a constant unidirectional acceleration to every object in the
    /// world, such as the gravitational acceleration on the surface of a planet.
    /// </summary>
    public class UniformForceField : ForceField
    {
        /// <summary>
        /// The default acceleration of uniform force field
        /// </summary>
        public const double DEFAULT_ACCELERATION = 9.80665;

        /// <summary>
        /// The constant acceleration this force field imposes on objects.
        /// </summary>
        public Vector Acceleration { get; set; }

        /// <summary>
        /// Creates a new uniform force field with the specified constant acceleration.
        /// </summary>
        /// <param name="ax">The x-component of the acceleration this force field imposes.</param>
        /// <param name="ay">The y-component of the acceleration this force field imposes.</param>
        /// <param name="az">The z-component of the acceleration this force field imposes.</param>
        public UniformForceField(double ax, double ay, double az)
        {
            Acceleration = new Vector(ax, ay, az);
        }

        /// <summary>
        /// Updates the given net force by the force that the implementing force field applies to
        /// the specified object. The force is given by the constant acceleration vector times the
        /// object's mass.
        /// </summary>
        /// <param name="netForce">The net force that will be updated.</param>
        /// <param name="obj">The object for which the force is calculated.</param>
        public override void UpdateForce(ref Vector netForce, PhysicsObject obj)
        {
            netForce += Acceleration * obj.Mass;
        }
    }
}