﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SharpDX;
using Matrix11 = SharpDX.Matrix;
using System.Diagnostics;

namespace NewtonLab
{
    class Camera
    {
        /* Sensitivity Constants */
        const float XRatio = 0.002f;
        const float YRatio = 0.002f;
        Vector3 WORLD_RIGHT = new Vector3(1.0f, 0.0f, 0.0f);
        Vector3 WORLD_UP = new Vector3(0.0f, 1.0f, 0.0f);
        Vector3 WORLD_FORWARD = new Vector3(0.0f, 0.0f, 1.0f);

        public Matrix11 View { get; private set; }
        public Vector3 Position { get; private set; }
        public Vector3 LookAt { get; private set; }
        public Vector3 Up { get; private set; }
        public Vector3 Right { get; private set; }

        public Camera()
        {
            Position = new Vector3(0, 1.5f, -5);
            LookAt = new Vector3(0, 0, 1);
            Up = new Vector3(0, 1, 0);
            Right = Vector3.Cross(Up, LookAt);

            CalculateViewMatrix();         
        }

        private void CalculateViewMatrix()
        {
            Matrix11 newView = View;

            float x = -Vector3.Dot(Right, Position);
            float y = -Vector3.Dot(Up, Position);
            float z = -Vector3.Dot(LookAt, Position);

            newView.Column1 = new Vector4(Right.X, Right.Y, Right.Z, x);
            newView.Column2 = new Vector4(Up.X, Up.Y, Up.Z, y);
            newView.Column3 = new Vector4(LookAt.X, LookAt.Y, LookAt.Z, z);
            newView.Column4 = new Vector4(0, 0, 0, 1.0f);

            View = newView;
        }

        /// <summary>
        /// Move the camear sideways.
        /// </summary>
        /// <param name="units">The amount to move the camera.</param>
        public void Strafe(float units)
        {
            Position += Right * units;

            CalculateViewMatrix();
        }

        /// <summary>
        /// Move the camera left or right.
        /// </summary>
        /// <param name="angle">Amount to move </param>
        public void Yaw(float angle)
        {
            Matrix11 T;
            angle *= XRatio;

            /* Create rotation matrix */
            T = Matrix11.RotationAxis(WORLD_UP, angle);

            /* Check if looking vertically up */
            if (Math.Abs(LookAt.Y) < 1.0f)
            {
                /* Not looking vertically up, evaluate normally */
                LookAt = Vector3.TransformCoordinate(LookAt, T);
                LookAt = Vector3.Normalize(LookAt);
                Up = WORLD_UP - LookAt.Y * LookAt;
            }
            else
            {
                /* Vertically Up, evaluate only up vector */
                Up = Vector3.TransformCoordinate(Up, T);
            }

            /* Find new Right vector */
            Up = Vector3.Normalize(Up);
            Right = Vector3.Cross(Up, LookAt);
            Right = Vector3.Normalize(Right);

            CalculateViewMatrix();
        }

        /// <summary>
        /// Look up or down.
        /// </summary>
        /// <param name="angle">The angle to pitch the camera.</param>
        public void Pitch(float angle)
        {
            if (angle == 0)
            {
                return;
            }

            Matrix11 T;

            angle *= YRatio;

            /* Create rotation matrix over Right vector */
            T = Matrix11.RotationAxis(Right, angle);

            /* Find new look at vector */
            LookAt = Vector3.TransformCoordinate(LookAt, T);
            LookAt = Vector3.Normalize(LookAt);

            /* Find new Up vector */
            Vector3 newUp = WORLD_UP - LookAt.Y * LookAt;

           

            /* Check if pitched so far as to become upside down */
            if (Vector3.Dot(Up, newUp) > 0.0f)
            {
                /* If not, set new Up */
                
                Up = newUp;
            }
            else
            {
                /* Went too far, flip around */
                
                LookAt = angle < 0 ? WORLD_UP : -WORLD_UP;
                Up = new Vector3(Up.X, 0.0f, Up.Z);
            }

            /* Find new Right vector */
            Up = Vector3.Normalize(Up);
            Right = Vector3.Cross(Up, LookAt);
            Right = Vector3.Normalize(Right);

            CalculateViewMatrix();
        }

        /// <summary>
        /// No longer used.
        /// </summary>
        /// <param name="angle">The angle to roll the camera.</param>
        public void Roll(float angle)
        {
            Matrix11 T;

            angle *= YRatio;

            T = Matrix11.RotationAxis(LookAt, angle);
            // rotate _up and _right around _look vector
            Right = Vector3.TransformCoordinate(Right, T);
            Up = Vector3.TransformCoordinate(Up, T);

            CalculateViewMatrix();
        }

        /// <summary>
        /// Move camera forward or backwards.
        /// </summary>
        /// <param name="units">Amount to move. </param>
        public void Walk(float units)
        {
            Position += LookAt * units;

            CalculateViewMatrix();
        }

        /// <summary>
        /// Move camera up or down.
        /// </summary>
        /// <param name="units">Amount to move. </param>
        public void Fly(float units)
        {
            Position += WORLD_UP * units;

            CalculateViewMatrix();
        }

        /// <summary>
        /// Gets the position in from of the Camera by input units.
        /// </summary>
        /// <param name="units">The distance in front of the camera. </param>
        /// <returns> Returns the position in front of the camera as Vector3. </returns>
        public Vector3 GetPositionInFront(float units)
        {
            return Position + (Vector3.Normalize(LookAt) * units);
        }
    }
}
