﻿using SharpDX.DirectInput;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewtonLab
{
    class KeyboardHandler
    {
        Keyboard keyboard;
        public KeyboardState keyboardState {get; private set;}
        KeyboardState previousState;

        public KeyboardHandler(DirectInput di)
        {
            keyboard = new Keyboard(di);

            /* Initialize States */
            keyboard.Acquire();
            keyboardState = keyboard.GetCurrentState();
            previousState = keyboardState;
        }

        /// <summary>
        /// Updates the state of the keyboard device.
        /// </summary>
        public void Update()
        {
            /* Store as previous state */
            previousState = keyboardState;

            /* Get new mouse states */
            keyboard.Acquire();
            keyboardState = keyboard.GetCurrentState();
        }

        /// <summary>
        /// Returns true if the key is down.
        /// </summary>
        /// <param name="key">The key code of the key to check. </param>
        /// <returns> Returns true if the key is down. </returns>
        public bool GetKey(Key key)
        {
            bool result = keyboardState.IsPressed(key);

            return result;
        }

        /// <summary>
        /// Returns true when the button has just been pressed.
        /// </summary>
        /// <param name="key">The code of the key. </param>
        /// <returns> Returns true when the button has just been pressed. </returns>
        public bool GetKeyDown(Key key)
        {
            bool result = keyboardState.IsPressed(key) && !previousState.IsPressed(key);

           
            return result;
        }

       
    }
}
