﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;

/* SharpDX Libraries */
using SharpDX;
using SharpDX.Windows;  /* RenderForm */
using SharpDX.Direct3D;
using SharpDX.Direct3D11;
using SharpDX.DXGI;
using SharpDX.RawInput;
using SharpDX.DirectInput;
using SharpDX.Multimedia;
using SharpDX.Win32;

using FactoryDXGI = SharpDX.DXGI.Factory1;
using NewtonLab.Graphics;
using Rectangle = System.Drawing.Rectangle;
using Point = System.Drawing.Point;
using NewtonEngine;
using NewtonEngine.Matrices;
using NewtonEngine.ForceFields;
using Panel = NewtonLab.Graphics.GUI.Panel;
using TextBox = NewtonLab.Graphics.GUI.TextBox;
using NewtonLab.Graphics.GUI;
using Button = NewtonLab.Graphics.GUI.Button;
using System.Runtime.InteropServices;
using NewtonEngine.Objects;
using Color11 = SharpDX.Color;
using Switch = NewtonLab.Graphics.GUI.Switch;
using SphereType = NewtonLab.GameObject.SphereType;

namespace NewtonLab
{
    class Game
    {
        #region Constants 
        /* Window Size */
        private int WIDTH = Screen.PrimaryScreen.WorkingArea.Width;
        private int HEIGHT = Screen.PrimaryScreen.WorkingArea.Height;
        private const bool FULLSCREEN = false;
        const float MILS_PER_SEC = 1000;
        const float FRAMES_PER_SEC = 60;    /* Graphics update rate */
        const long MILS_PER_FRAME = (long)(MILS_PER_SEC / FRAMES_PER_SEC);

        const float PHYS_FRAMES_PER_SEC = 120;     /* Physics update rate */
        const long PHYS_MILS_PER_FRAME = (long)(MILS_PER_SEC / PHYS_FRAMES_PER_SEC);

        private const float DISTANCE_TO_SPAWN = 1.25f;   /* The distance from the camera to spawn game objects */

        /* Movement Speeds */
        public const float WALK_SPEED = 0.001f; /* Walking speed in meters per millisecond */
        public const float RUN_SPEED = 0.01242f; /* Usain Bolt running speed */

        #endregion

        /* Rendering form */
        private static RenderForm renderForm;

        public static bool cameraMode = false;

        /* Game Timer */
        Stopwatch gameTimer = Stopwatch.StartNew();

        /* Physics Timer */
        Stopwatch physicsTimer;
        long physicsTimePassed = 0;

        /* Frame rate */
        Stopwatch frameTimer = Stopwatch.StartNew();
        long physicsFrameRate = 0;
        long graphicsFrameRate = 0;

        #region Throwing Elements 
        Stopwatch throwTimer;
        private const int THROW_MIN_TIME = 500;
        private const float THROW_SPEED = 0.03f;  /* Throw speed per milisecond held */
        Vector3 ThrowVelocity = new Vector3();
        #endregion

        /* Mouse points tracking */
        Point p;
        Point px;

        /* Selected model to spawn */
        string SelectedModel = null;

        /* Possible selections to spawn */
        Dictionary<Key, string> selectableModels = new Dictionary<Key, string>();
        Key[] spawnKeys = { Key.D1, Key.D2, Key.D3, Key.D4, Key.D5, Key.D6, Key.D7, Key.D8, Key.D9, Key.D0 }; /* Spawning Keys */

        #region Text Input dictionary 
        /* Keys and their values */
        Dictionary<Key, string> InputTextDictionary = new Dictionary<Key, string>();
        #endregion

        #region Game Components
        public GraphicsHandler GraphicsHandler { get; private set; }
        public GUIRenderer GUIRenderer { get; private set; }
        public Camera Camera { get; private set; }
        public MouseHandler MouseHandler { get; private set; }
        public KeyboardHandler KeyboardHandler { get; private set; }
        public PhysicsEngine PhysicsEngine { get; private set; }
        public ObjectPicker ObjectPicker { get; private set; }
        #endregion

        #region Game Object Instances 
        GameObject heldObject;   /* The object that is being held */
        #endregion

        #region Forcefield Instances
        UniformForceField gravity;
        Drag drag;
        RandomForceField randomForceField;
        RollingResistance rollingResistance;
        #endregion

        #region GUI elements 
        Panel performancePanel;
        Panel csPanel;
        Panel invPanel;
        Panel settingsPanel;
        Panel playPausePanel;
        Panel LegendPanel;

        public VectorPainter VectorPainter { get; private set; }
        #endregion

        #region parameterPanel and its textbox 

        Panel parameterPanel;
        TextBox physicsFPSPanel;
        TextBox graphicsFPSPanel;
        TextBox nameText;
        TextBox massText;
        TextBox densityText;
        TextBox densityNA;
        TextBox posXText;
        TextBox posYText;
        TextBox posZText;
        TextBox rotXLabel;
        TextBox rotYLabel;
        TextBox rotZLabel;
        TextBox rotXText;
        TextBox rotYText;
        TextBox rotZText;
        TextBox rotNA;
        TextBox scaleLabel;
        TextBox radiusLabel;
        TextBox scaleXLabel;
        TextBox scaleYLabel;
        TextBox scaleZLabel;
        TextBox scaleXText;
        TextBox scaleYText;
        TextBox scaleZText;
        TextBox velLabelX;
        TextBox velLabelY;
        TextBox velLabelZ;
        TextBox velXText;
        TextBox velYText;
        TextBox velZText;
        TextBox velNA;
        TextBox velMag;
        TextBox velMagNA;
        TextBox settingsButt;
        Switch centForceSwitch;
        TextBox centForceText;
        TextBox centForceNA;

        Button resetButton;
        Button deleteButton;
        Button fixButton;
        

        #endregion

        #region Settings Panel
        TextBox settingsText;
        TextBox gravityXText, gravityYText, gravityZText;
        TextBox dragText;
        TextBox restitutionText;
        TextBox randomForceText;
        TextBox rollingFrictionText;

        Switch rollingFrictionSwitch;
        Switch gravitySwitch;
        Switch dragSwitch;
        Switch randomForceSwitch;

        #endregion

        #region Inventory Panel
        TextBox sphereInv;
        TextBox blockInv;
        TextBox carInv;
        TextBox raccoonInv;
        TextBox tennisInv;
        TextBox brickInv;
       
        TextBox blankInv3;
        TextBox blankInv4;
        TextBox blankInv5;
        TextBox blankInv6;
        TextBox blankInv7;
        
       
        #endregion

        #region PlayPlayPanel
        Button pauseButton;
        #endregion

        GUIObject selectedGUI = null;
        bool holdingMode = false;
        bool UIMode = false;
        bool PauseMode = true;
        bool ContinuousMode = false;  

        public Game()
        {
            Initialize();
        }

        /// <summary>
        /// Starts the game.
        /// </summary>
        public void Start()
        {
            /* Start physics timer */
            physicsTimer = Stopwatch.StartNew();

            /* Runs the RenderCallBack() function */
            RenderLoop.Run(renderForm, RenderCallBack);
        }

        /// <summary>
        /// Initializes all the game components.
        /// </summary>
        public void Initialize()
        {
            /* Create Render Form */
            renderForm = new RenderForm("Physics Engine");

            /* Set Window Parameters */
            renderForm.StartPosition = FormStartPosition.CenterScreen;
            renderForm.WindowState = FormWindowState.Maximized;

            /* Disable minimize and maximize buttons */
            renderForm.MinimizeBox = false;
            //renderForm.MaximizeBox = false;

            /* Initialize Graphics Handler */
            GraphicsHandler = new GraphicsHandler(renderForm);
            GameObject.DefaultGraphicsHandler = GraphicsHandler;
            GUIObject.DefaultGraphicsHandler = GraphicsHandler;
            GUIObject.DefaultRenderForm = renderForm;
            /* Store GUI Renderer */
            GUIRenderer = GraphicsHandler.GUIRenderer;

            /* Initialize Physics Engine */
            PhysicsEngine = new PhysicsEngine();
            GameObject.DefaultPhysicsEngine = PhysicsEngine;
            PhysicsEngine.RestitutionCoefficient = 0.85;

            /* Initialize the Object Picker */
            ObjectPicker = new ObjectPicker();

            /* Create Camera object */
            Camera = new Camera();

            /* Populate Game Object Library */
            PopulateGameObjectLibrary();

            /* Populate text input dictionary */
            PopulateInputTextDictionary();

            /* Create Game Objects */
            CreateGameObjects();

            /* Create Force Fields */
            CreateForceFields();

            /* Create GUI Elements */
            CreateGUIElements();

            /* Set up Spawn Options */
            SetUpSpawnOptions();

            /* Create Input Handler Objects */
            CreateInputHandlers();
        }

        /// <summary>
        /// This function is called every update in the Renderloop.
        /// </summary>
        private void RenderCallBack()
        {
            if (gameTimer.ElapsedMilliseconds > MILS_PER_FRAME)
            {
                GraphicsHandler.Frame(Camera);

                ++graphicsFrameRate;

                if (ApplicationIsActivated())
                {
                    HandleInputs(gameTimer.ElapsedMilliseconds);
                }
                /* Reset Timer */
                gameTimer = Stopwatch.StartNew();
            }


            /*if a pausemode is on, the timer wil be stopped and reseted, else, it will restart the timer and go throught the physics engine update*/

            if (PauseMode && !ContinuousMode)
            {
                physicsTimer.Stop();
                physicsTimer.Reset();
            }
            else
            {
                
                physicsTimer.Start();
                physicsTimePassed = physicsTimer.ElapsedMilliseconds;

                if (physicsTimer.ElapsedMilliseconds > PHYS_MILS_PER_FRAME)
                {

                    PhysicsEngine.Update(Camera.Position.X, Camera.Position.Y, Camera.Position.Z,
                        physicsTimePassed);

                    GameObject.CleanUpDistanceObjects(Camera);

                    ++physicsFrameRate;

                    physicsTimer = Stopwatch.StartNew();
                }
            }
            

            /* Reset and display performance data every second */
            if (frameTimer.ElapsedMilliseconds > 1000)
            {
               physicsFPSPanel.Text = physicsFrameRate.ToString();
               graphicsFPSPanel.Text = graphicsFrameRate.ToString();

               physicsFrameRate = 0;
               graphicsFrameRate = 0;
               frameTimer = Stopwatch.StartNew();
            }

            /* UI mode for updating the field thorughout the run of the program. */
            if (!UIMode)
            {
                /* If the user is holding/selected the object, it iwll show the panel and its textboxes*/
                if (heldObject != null)
                {

                    DisplayParametersPanel();

                    nameText.Text = heldObject.Name;
                    massText.Text = Math.Round(heldObject.Mass, 2).ToString();
                    posXText.Text = Math.Round(heldObject.Position.X, 2).ToString();
                    posYText.Text = Math.Round(heldObject.Position.Y, 2).ToString();
                    posZText.Text = Math.Round(heldObject.Position.Z, 2).ToString();
                    
                    /* If throwing, show stored velocity */
                    if (ThrowVelocity != Vector3.Zero)
                    {
                        velXText.Text = Math.Round(ThrowVelocity.X, 2).ToString();
                        velYText.Text = Math.Round(ThrowVelocity.Y, 2).ToString();
                        velZText.Text = Math.Round(ThrowVelocity.Z, 2).ToString();
                        velMag.Text = Math.Round(Math.Sqrt(Math.Pow(Convert.ToDouble(velXText.Text), 2) + Math.Pow(Convert.ToDouble(velYText.Text), 2) + Math.Pow(Convert.ToDouble(velZText.Text), 2)),2).ToString();
                    }
                    else
                    {
                        velXText.Text = Math.Round(heldObject.Velocity.X, 2).ToString();
                        velYText.Text = Math.Round(heldObject.Velocity.Y, 2).ToString();
                        velZText.Text = Math.Round(heldObject.Velocity.Z, 2).ToString();
                        velMag.Text = heldObject.PhysicsObject.Velocity.GetLength().ToString();
                    }

                    rotXText.Text = Math.Round(heldObject.PhysicsObject.Rotation.X * 180 / Math.PI, 2).ToString();
                    rotYText.Text = Math.Round(heldObject.PhysicsObject.Rotation.Y * 180 / Math.PI, 2).ToString();
                    rotZText.Text = Math.Round(heldObject.PhysicsObject.Rotation.Z * 180 / Math.PI, 2).ToString();
                        
                    //Scaling limit check
                    for(int i = 0; i < 3; i++)
                    {
                        if(heldObject.PhysicsObject.Scaling[i] > 4.0)
                        {
                            heldObject.PhysicsObject.Scaling[i] = 4.0;
                        }
                        else if(heldObject.PhysicsObject.Scaling[i] < 0.01)
                        {
                            heldObject.PhysicsObject.Scaling[i] = 0.01;
                        }
                    }
                    scaleXText.Text = Math.Round(heldObject.PhysicsObject.Scaling.X,2).ToString();  
                    scaleYText.Text = Math.Round(heldObject.PhysicsObject.Scaling.Y, 2).ToString();
                    scaleZText.Text = Math.Round(heldObject.PhysicsObject.Scaling.Z, 2).ToString();

                    /* Display centric forcefield info */
                    if (heldObject.PhysicsObject.ForceField != null)
                    {
                        if (heldObject.PhysicsObject.ForceField.Enabled)
                        {
                            centForceSwitch.on();
                        }
                        else
                        {
                            centForceSwitch.off();
                        }

                        centForceText.Text = heldObject.PhysicsObject.ForceField.Acceleration.ToString();
                    }
                    else
                    {
                        centForceSwitch.off();
                        centForceText.Text = "-1";
                    }

                    /* if the object is collidable, it will show the density of it */
                    if (heldObject.PhysicsObject is Collidable)
                    {
                        Collidable collidable = heldObject.PhysicsObject as Collidable;
                        densityText.Text = Math.Round(collidable.Density,2).ToString();
                    }

                    if (heldObject.Fixed == true)
                        resetButton.Hide();
                }
                else
                {
                    nameText.Text = "";
                    parameterPanel.Hide();
                }
            }

        }

        /// <summary>
        /// Populate the game object library.
        /// </summary>
        private void PopulateGameObjectLibrary()
        {
            GameObject.AddToLibrary("Sphere", GameObject.CreateSphere(Vector3.Zero, 0.12f, SphereType.Normal, true, false));
            GameObject.AddToLibrary("Block", GameObject.CreateBlock(Vector3.Zero, 0.25, GameObject.BlockType.Normal, true, false));
            GameObject.AddToLibrary("Dog", GameObject.CreateObject(Vector3.Zero, "./Models/dog/dog.obj", "Dog", false));
            
            GameObject tempRaccoon = GameObject.CreateObject(Vector3.Zero, "./Models/Raccoon/raccoon.obj", "Raccoon", false);
            tempRaccoon.PhysicsObject.Scale(0.1, 0.1, 0.1);
            
            GameObject.AddToLibrary("Raccoon", tempRaccoon);
            GameObject.AddToLibrary("Basketball", GameObject.CreateSphere(Vector3.Zero, 0.125f, SphereType.Basketball, true, false));
            GameObject.AddToLibrary("Tennisball", GameObject.CreateSphere(Vector3.Zero, 0.04f, SphereType.Tennisball, true, false));
            
            GameObject tempBrickWall = GameObject.CreateBlock(Vector3.Zero, 1, GameObject.BlockType.BrickWall, true, false);
            tempBrickWall.PhysicsObject.Scale(4, 2, 0.2);
            GameObject.AddToLibrary("Brick Wall", tempBrickWall);
        }

        private void PopulateInputTextDictionary()
        {
            Key[] pressed = { Key.D0, Key.D1, Key.D2, Key.D3, Key.D4, Key.D5, Key.D6, Key.D7, Key.D8, Key.D9, 
                                Key.NumberPad0, Key.NumberPad1, Key.NumberPad2, Key.NumberPad3, Key.NumberPad4, Key.NumberPad5, Key.NumberPad6, Key.NumberPad7, 
                                Key.NumberPad8, Key.NumberPad9, Key.Minus,};


            InputTextDictionary.Add(Key.D0, "0");
            InputTextDictionary.Add(Key.D1, "1");
            InputTextDictionary.Add(Key.D2, "2");
            InputTextDictionary.Add(Key.D3, "3");
            InputTextDictionary.Add(Key.D4, "4");
            InputTextDictionary.Add(Key.D5, "5");
            InputTextDictionary.Add(Key.D6, "6");
            InputTextDictionary.Add(Key.D7, "7");
            InputTextDictionary.Add(Key.D8, "8");
            InputTextDictionary.Add(Key.D9, "9");

            InputTextDictionary.Add(Key.NumberPad0, "0");
            InputTextDictionary.Add(Key.NumberPad1, "1");
            InputTextDictionary.Add(Key.NumberPad2, "2");
            InputTextDictionary.Add(Key.NumberPad3, "3");
            InputTextDictionary.Add(Key.NumberPad4, "4");
            InputTextDictionary.Add(Key.NumberPad5, "5");
            InputTextDictionary.Add(Key.NumberPad6, "6");
            InputTextDictionary.Add(Key.NumberPad7, "7");
            InputTextDictionary.Add(Key.NumberPad8, "8");
            InputTextDictionary.Add(Key.NumberPad9, "9");

            InputTextDictionary.Add(Key.Minus, "-");

        }

        /// <summary>
        /// Initialize the game objects of the lab.
        /// </summary>
        private void CreateGameObjects()
        {
            CreateFloor();
        }

        private void CreateFloor()
        {
            GameObject.AddToLibrary("Ground", GameObject.CreateBlock(Vector3.Zero, 50, 10, 50, GameObject.BlockType.Floor, false, false));
            
            /* Creates multiple pieces of the floor */
            //for (int i = 0; i <= 1; ++i)
            //{
            //    for (int j = 0; j <= 1; ++j)
            //    {
            //        //GameObject.CreateBlock(new Vector3(-5 + i, -5,-5 + j), 1, 10, 1, GameObject.BlockType.Raccoon, false);
            //        GameObject.CreateByName("Ground", new Vector3(-50 + (i * 50), -5, -50 + (j * 50)), false, false);
            //    }
            //}

            GameObject.CreateByName("Ground", new Vector3(0 , -5, 0), false, false);
        }
        
        /// <summary>
        /// Creating random force field.
        /// </summary>
        private void CreateForceFields()
        {
            /* ForceField Instances */
            gravity = new UniformForceField(0, -UniformForceField.DEFAULT_ACCELERATION, 0);
            PhysicsEngine.AddForceField(gravity);
            gravity.Enabled = true;

            randomForceField = new RandomForceField(10, 500);
            PhysicsEngine.AddForceField(randomForceField);
            
            drag = new Drag();
            PhysicsEngine.AddForceField(drag);
            drag.Enabled = true;

            rollingResistance = new RollingResistance();
            PhysicsEngine.AddForceField(rollingResistance);
            rollingResistance.Enabled = true;
        }

        #region UI Creation
        /// <summary>
        /// Create the gui elements to be drawn.
        /// </summary>
        private void CreateGUIElements()
        {
            //crosshair panel.
            csPanel = new Panel(new Vector2(renderForm.Width/2, renderForm.Height/2), GraphicsHandler);
            csPanel.AddObject(new Crosshair("./Sprites/Crosshair.gif", 0.8f));
            csPanel.Hide();

            CreateParamPanel();

            // Inventory Panel and its textbox/panel
            invPanel = new Panel(new Vector2(renderForm.Width / 2 - 243, renderForm.Height - 85), GraphicsHandler, "./Sprites/panelPSD.gif");
            raccoonInv = new TextBox(new Vector2(renderForm.Width / 2 - 236, renderForm.Height - 78), Color11.Black, null, "./Sprites/raccooninv_textbox.bmp", null, false);
            carInv = new TextBox(new Vector2(renderForm.Width / 2 - 193, renderForm.Height - 78), Color11.Black, null, "./Sprites/basketballinv_textbox.bmp", null, false);
            sphereInv = new TextBox(new Vector2(renderForm.Width / 2 - 150, renderForm.Height - 78), Color11.Black, null, "./Sprites/sphereinv_textbox.bmp", null, false);
            blockInv = new TextBox(new Vector2(renderForm.Width / 2 - 107, renderForm.Height - 78), Color11.Black, null, "./Sprites/blockinv_textbox.bmp", null, false);
            tennisInv = new TextBox(new Vector2(renderForm.Width / 2 - 64, renderForm.Height - 78), Color11.Black, null, "./Sprites/tennisinv_textbox.bmp", null, false);
            
            brickInv = new TextBox(new Vector2(renderForm.Width / 2 - 21, renderForm.Height - 78), Color11.Black, null, "./Sprites/brickinv_textbox.bmp", null, false);
            blankInv3 = new TextBox(new Vector2(renderForm.Width / 2 + 21, renderForm.Height - 78), Color11.Black, null, "./Sprites/inv_textbox.bmp", null, false);
            blankInv4 = new TextBox(new Vector2(renderForm.Width / 2 + 64, renderForm.Height - 78), Color11.Black, null, "./Sprites/inv_textbox.bmp", null, false);
            blankInv5 = new TextBox(new Vector2(renderForm.Width / 2 + 107, renderForm.Height - 78), Color11.Black, null, "./Sprites/inv_textbox.bmp", null, false);
            blankInv6 = new TextBox(new Vector2(renderForm.Width / 2 + 150, renderForm.Height - 78), Color11.Black, null, "./Sprites/inv_textbox.bmp", null, false);
            blankInv7 = new TextBox(new Vector2(renderForm.Width / 2 + 193, renderForm.Height - 78), Color11.Black, null, "./Sprites/inv_textbox.bmp", null, false);
            settingsButt = new TextBox(new Vector2(renderForm.Width - 60, renderForm.Height - 80), Color11.Black, null, "./Sprites/settingsbut.gif", null, false);
            invPanel.AddObject(raccoonInv);
            invPanel.AddObject(sphereInv);
            invPanel.AddObject(carInv);
            invPanel.AddObject(blockInv);
            invPanel.AddObject(tennisInv);
            invPanel.AddObject(brickInv);
            invPanel.AddObject(blankInv3);
            invPanel.AddObject(blankInv4);
            invPanel.AddObject(blankInv5);
            invPanel.AddObject(blankInv6);
            invPanel.AddObject(blankInv7);
            invPanel.AddObject(settingsButt);
            
            /* Settings Panel which includes gravity and airdrag settings. */
            settingsPanel = new Panel(new Vector2(renderForm.Width - 440, renderForm.Height  - 260), GraphicsHandler, "./Sprites/panelsettings.gif");

            settingsText = new TextBox(new Vector2(renderForm.Width - 430, renderForm.Height - 250), Color11.WhiteSmoke, "Settings");
            settingsText.txtScale = 0.8f;
            settingsPanel.AddObject(settingsText);

            //Gravity Labels
            settingsPanel.AddObject(new TextBox(new Vector2(renderForm.Width - 430, renderForm.Height - 220), Color11.WhiteSmoke, "Gravity, Directional Force Strength (m/s^2)"));
            settingsPanel.AddObject(new TextBox(new Vector2(renderForm.Width - 390, renderForm.Height - 190), Color11.WhiteSmoke, "X"));
            settingsPanel.AddObject(new TextBox(new Vector2(renderForm.Width - 310, renderForm.Height - 190), Color11.WhiteSmoke, "Y"));
            settingsPanel.AddObject(new TextBox(new Vector2(renderForm.Width - 230, renderForm.Height - 190), Color11.WhiteSmoke, "Z"));

            //Random Force Labels
            settingsPanel.AddObject(new TextBox(new Vector2(renderForm.Width - 430, renderForm.Height - 160), Color11.WhiteSmoke, "Random Force Strength (m/s^2)"));
            
            //Drag Labels
            settingsPanel.AddObject(new TextBox(new Vector2(renderForm.Width - 430, renderForm.Height - 130), Color11.WhiteSmoke, "Fluid Density for Drag (kg/m^3)"));

            //Rolling Friction Labels
            settingsPanel.AddObject(new TextBox(new Vector2(renderForm.Width - 430, renderForm.Height - 100), Color11.WhiteSmoke, "Rolling Resistance Coefficient"));

            //Restitution Labels
            settingsPanel.AddObject(new TextBox(new Vector2(renderForm.Width - 430, renderForm.Height - 70), Color11.WhiteSmoke, "Restitution Coefficient"));

            //Gravity Text/Switch
            gravitySwitch = new Switch(new Vector2(renderForm.Width - 140, renderForm.Height - 190), null, "./Sprites/switch_off.gif");
            gravityXText = new TextBox(new Vector2(renderForm.Width - 370, renderForm.Height - 190), Color11.Black, Math.Round(gravity.Acceleration[0], 2).ToString(), "./Sprites/textBoxmed.bmp", "./Sprites/textBoxmedClicked.bmp");
            gravityYText = new TextBox(new Vector2(renderForm.Width - 290, renderForm.Height - 190), Color11.Black, Math.Round(gravity.Acceleration[1], 2).ToString(), "./Sprites/textBoxmed.bmp", "./Sprites/textBoxmedClicked.bmp");
            gravityZText = new TextBox(new Vector2(renderForm.Width - 210, renderForm.Height - 190), Color11.Black, Math.Round(gravity.Acceleration[2], 2).ToString(), "./Sprites/textBoxmed.bmp", "./Sprites/textBoxmedClicked.bmp");

            //Random Force Input/Switch
            randomForceText = new TextBox(new Vector2(renderForm.Width - 210, renderForm.Height - 160), Color11.Black, Math.Round(randomForceField.Acceleration, 2).ToString(), "./Sprites/textBoxmed.bmp", "./Sprites/textBoxmedClicked.bmp");
            randomForceSwitch = new Switch(new Vector2(renderForm.Width - 140, renderForm.Height - 160), null, "./Sprites/switch_off.gif");
            
            //Drag Input/Switch
            dragText = new TextBox(new Vector2(renderForm.Width - 210, renderForm.Height - 130), Color11.Black, Math.Round(drag.Density).ToString(), "./Sprites/textBoxmed.bmp", "./Sprites/textBoxmedClicked.bmp");
            dragSwitch = new Switch(new Vector2(renderForm.Width - 140, renderForm.Height - 130), null, "./Sprites/switch_off.gif");
            
            //Rolling Friction Input/Switch
            rollingFrictionText = new TextBox(new Vector2(renderForm.Width - 210, renderForm.Height - 100), Color11.Black, Math.Round(rollingResistance.RollingCoefficient, 2).ToString(), "./Sprites/textBoxmed.bmp", "./Sprites/textBoxmedClicked.bmp");
            rollingFrictionSwitch = new Switch(new Vector2(renderForm.Width - 140, renderForm.Height - 100), null, "./Sprites/switch_off.gif");

            //Restitution Input/Switch
            restitutionText = new TextBox(new Vector2(renderForm.Width - 210, renderForm.Height - 70), Color11.Black, Math.Round(PhysicsEngine.RestitutionCoefficient, 2).ToString(), "./Sprites/textBoxmed.bmp", "./Sprites/textBoxmedClicked.bmp");

            gravitySwitch.on();
            dragSwitch.on();
            rollingFrictionSwitch.on();

            settingsPanel.AddObject(gravityXText);
            settingsPanel.AddObject(gravityYText);
            settingsPanel.AddObject(gravityZText);
            settingsPanel.AddObject(dragText);
            settingsPanel.AddObject(gravitySwitch);
            settingsPanel.AddObject(dragSwitch);
            settingsPanel.AddObject(restitutionText);
            settingsPanel.AddObject(randomForceSwitch);
            settingsPanel.AddObject(randomForceText);
            settingsPanel.AddObject(rollingFrictionText);
            settingsPanel.AddObject(rollingFrictionSwitch);
            //settingsPanel.Hide();

            /* Play and pause panel to pause game.*/
            playPausePanel = new Panel(new Vector2(5, 5), GraphicsHandler);
            pauseButton = new Button(new Vector2(10, 10), null, "./Sprites/pauseButton.gif", "./Sprites/playButton.gif");
            playPausePanel.AddObject(pauseButton);
            pauseButton.on();

            /* Legend Label*/
            LegendPanel = new Panel(new Vector2(renderForm.Width / 2 - 420, 0), GraphicsHandler, "./Sprites/legendpanel.gif");
            LegendPanel.AddObject(new TextBox(new Vector2(renderForm.Width / 2 - 420, 5), Color11.WhiteSmoke, "C - Camera Mode   P - Pause Mode    G - Gravity Toggle    R - Random Force Field Toggle    Delete - Delete Selected Object     I - Drag Toggle"));

            

            /* Create the vector painter */
            VectorPainter = GUIRenderer.CreateVectorPainter(Camera);
        }

        private void CreateParamPanel()
        {
            //Performance panel which shows FPS for both graphics and physics.
            performancePanel = new Panel(new Vector2(renderForm.Width - 150, 0), GraphicsHandler);
            physicsFPSPanel = new TextBox(new Vector2(renderForm.Width - 55, 10), Color11.Yellow);
            physicsFPSPanel.txtScale = 1.0f;
            performancePanel.AddObject(physicsFPSPanel);
            graphicsFPSPanel = new TextBox(new Vector2(renderForm.Width - 55, 30), Color11.Red);
            graphicsFPSPanel.txtScale = 1.0f;
            performancePanel.AddObject(graphicsFPSPanel);

            //Parameter panel which shows detail of the object position, mass, velocity, density.
            parameterPanel = new Panel(new Vector2(5, renderForm.Height - 375), GraphicsHandler, "./Sprites/panel_leftbot.gif");

            nameText = new TextBox(new Vector2(10, renderForm.Height - 365), Color11.WhiteSmoke, "");
            nameText.txtScale = 0.8f;
            parameterPanel.AddObject(nameText);

            //Mass label.
            massText = new TextBox(new Vector2(140, renderForm.Height - 335), Color11.Black, " ", "./Sprites/textBoxmed.bmp", "./Sprites/textBoxmedClicked.bmp");
            parameterPanel.AddObject(massText);
            parameterPanel.AddObject(new TextBox(new Vector2(10, renderForm.Height - 335), Color11.WhiteSmoke, "Mass (kg)"));

            //Density label
            parameterPanel.AddObject(new TextBox(new Vector2(10, renderForm.Height - 305), Color11.WhiteSmoke, "Density (kg/m^3)"));
            densityText = new TextBox(new Vector2(140, renderForm.Height - 305), Color11.Black, " ", "./Sprites/textBoxmed.bmp", "./Sprites/textBoxmedClicked.bmp");
            densityNA = new TextBox(new Vector2(140, renderForm.Height - 305), Color11.WhiteSmoke, "N/A");
            parameterPanel.AddObject(densityText);
            parameterPanel.AddObject(densityNA);

            //Position Label and xyz labels
            parameterPanel.AddObject(new TextBox(new Vector2(10, renderForm.Height - 275), Color11.WhiteSmoke, "Position (m)"));
            parameterPanel.AddObject(new TextBox(new Vector2(120, renderForm.Height - 275), Color11.WhiteSmoke, "X"));
            parameterPanel.AddObject(new TextBox(new Vector2(200, renderForm.Height - 275), Color11.WhiteSmoke, "Y"));
            parameterPanel.AddObject(new TextBox(new Vector2(280, renderForm.Height - 275), Color11.WhiteSmoke, "Z"));
            posXText = new TextBox(new Vector2(140, renderForm.Height - 275), Color11.Black, " ", "./Sprites/textBoxmed.bmp", "./Sprites/textBoxmedClicked.bmp");
            posYText = new TextBox(new Vector2(220, renderForm.Height - 275), Color11.Black, " ", "./Sprites/textBoxmed.bmp", "./Sprites/textBoxmedClicked.bmp");
            posZText = new TextBox(new Vector2(300, renderForm.Height - 275), Color11.Black, " ", "./Sprites/textBoxmed.bmp", "./Sprites/textBoxmedClicked.bmp");
            parameterPanel.AddObject(posXText);
            parameterPanel.AddObject(posYText);
            parameterPanel.AddObject(posZText);

            //Velocity label and xyz labels
            parameterPanel.AddObject(new TextBox(new Vector2(10, renderForm.Height - 245), Color11.WhiteSmoke, "Velocity (m/s)"));
            velLabelX = new TextBox(new Vector2(120, renderForm.Height - 245), Color11.WhiteSmoke, "X");
            velLabelY = new TextBox(new Vector2(200, renderForm.Height - 245), Color11.WhiteSmoke, "Y");
            velLabelZ = new TextBox(new Vector2(280, renderForm.Height - 245), Color11.WhiteSmoke, "Z");
            velXText = new TextBox(new Vector2(140, renderForm.Height - 245), Color11.Black, " ", "./Sprites/textBoxmed.bmp", "./Sprites/textBoxmedClicked.bmp");
            velYText = new TextBox(new Vector2(220, renderForm.Height - 245), Color11.Black, " ", "./Sprites/textBoxmed.bmp", "./Sprites/textBoxmedClicked.bmp");
            velZText = new TextBox(new Vector2(300, renderForm.Height - 245), Color11.Black, " ", "./Sprites/textBoxmed.bmp", "./Sprites/textBoxmedClicked.bmp");
            velNA = new TextBox(new Vector2(140, renderForm.Height - 245), Color11.WhiteSmoke, "N/A");
            parameterPanel.AddObject(velLabelX);
            parameterPanel.AddObject(velLabelY);
            parameterPanel.AddObject(velLabelZ);
            parameterPanel.AddObject(velXText);
            parameterPanel.AddObject(velYText);
            parameterPanel.AddObject(velZText);
            parameterPanel.AddObject(velNA);

            //Speed label
            parameterPanel.AddObject(new TextBox(new Vector2(10, renderForm.Height - 215), Color11.WhiteSmoke, "Speed (m/s)"));
            velMag = new TextBox(new Vector2(140, renderForm.Height - 215), Color11.WhiteSmoke, "");
            velMagNA = new TextBox(new Vector2(140, renderForm.Height - 215), Color11.WhiteSmoke, "N/A");
            resetButton = new Button(new Vector2(300, renderForm.Height - 215), null, "./Sprites/resetButton.gif", "./Sprites/resetButtonClicked.gif");
            parameterPanel.AddObject(velMag);
            parameterPanel.AddObject(velMagNA);
            parameterPanel.AddObject(resetButton);

            //Scale label
            scaleLabel = new TextBox(new Vector2(10, renderForm.Height - 185), Color11.WhiteSmoke, "Dimensions (m)");
            radiusLabel = new TextBox(new Vector2(10, renderForm.Height - 185), Color11.WhiteSmoke, "Radius (m)");
            scaleXLabel = new TextBox(new Vector2(120, renderForm.Height - 185), Color11.WhiteSmoke, "X");
            scaleYLabel = new TextBox(new Vector2(200, renderForm.Height - 185), Color11.WhiteSmoke, "Y");
            scaleZLabel = new TextBox(new Vector2(280, renderForm.Height - 185), Color11.WhiteSmoke, "Z");
            scaleXText = new TextBox(new Vector2(140, renderForm.Height - 185), Color11.Black, " ", "./Sprites/textBoxmed.bmp", "./Sprites/textBoxmedClicked.bmp");
            scaleYText = new TextBox(new Vector2(220, renderForm.Height - 185), Color11.Black, " ", "./Sprites/textBoxmed.bmp", "./Sprites/textBoxmedClicked.bmp");
            scaleZText = new TextBox(new Vector2(300, renderForm.Height - 185), Color11.Black, " ", "./Sprites/textBoxmed.bmp", "./Sprites/textBoxmedClicked.bmp");
            parameterPanel.AddObject(scaleLabel);
            parameterPanel.AddObject(radiusLabel);
            parameterPanel.AddObject(scaleXLabel);
            parameterPanel.AddObject(scaleYLabel);
            parameterPanel.AddObject(scaleZLabel);
            parameterPanel.AddObject(scaleXText);
            parameterPanel.AddObject(scaleYText);
            parameterPanel.AddObject(scaleZText);

            //Rotation label 
            parameterPanel.AddObject(new TextBox(new Vector2(10, renderForm.Height - 155), Color11.WhiteSmoke, "Rotation (deg)"));
            rotXLabel = new TextBox(new Vector2(120, renderForm.Height - 155), Color11.WhiteSmoke, "X");
            rotYLabel = new TextBox(new Vector2(200, renderForm.Height - 155), Color11.WhiteSmoke, "Y");
            rotZLabel = new TextBox(new Vector2(280, renderForm.Height - 155), Color11.WhiteSmoke, "Z");
            rotXText = new TextBox(new Vector2(140, renderForm.Height - 155), Color11.Black, " ", "./Sprites/textBoxmed.bmp", "./Sprites/textBoxmedClicked.bmp");
            rotYText = new TextBox(new Vector2(220, renderForm.Height - 155), Color11.Black, " ", "./Sprites/textBoxmed.bmp", "./Sprites/textBoxmedClicked.bmp");
            rotZText = new TextBox(new Vector2(300, renderForm.Height - 155), Color11.Black, " ", "./Sprites/textBoxmed.bmp", "./Sprites/textBoxmedClicked.bmp");
            rotNA = new TextBox(new Vector2(140, renderForm.Height - 155), Color11.WhiteSmoke, "N/A");
            parameterPanel.AddObject(rotXLabel);
            parameterPanel.AddObject(rotYLabel);
            parameterPanel.AddObject(rotZLabel);
            parameterPanel.AddObject(rotXText);
            parameterPanel.AddObject(rotYText);
            parameterPanel.AddObject(rotZText);
            parameterPanel.AddObject(rotNA);

            /* Centric force switch */
            parameterPanel.AddObject(new TextBox(new Vector2(10, renderForm.Height - 125), Color11.WhiteSmoke, "Field Strength"));
            centForceText = new TextBox(new Vector2(140, renderForm.Height - 125), Color11.Black, " ", "./Sprites/textBoxmed.bmp", "./Sprites/textBoxmedClicked.bmp");
            centForceSwitch = new Switch(new Vector2(220, renderForm.Height - 125), null, "./Sprites/switch_off.gif", false);
            centForceNA = new TextBox(new Vector2(140, renderForm.Height - 125), Color11.WhiteSmoke, "N/A");
            parameterPanel.AddObject(centForceText);
            parameterPanel.AddObject(centForceSwitch);
            parameterPanel.AddObject(centForceNA);

            deleteButton = new Button(new Vector2(220, renderForm.Height -75), null, "./Sprites/deleteButton.gif", "./Sprites/deleteButtonClicked.gif");
            parameterPanel.AddObject(deleteButton);

            fixButton = new Button(new Vector2(300, renderForm.Height - 75), null, "./Sprites/fixButton.gif", "./Sprites/fixButtonClicked.gif");
            parameterPanel.AddObject(fixButton);

            parameterPanel.Hide();
        }

        #endregion

        #region Input Methods
        /// <summary>
        /// Create Input Handling objects.
        /// </summary>
        public void CreateInputHandlers()
        {
            /*** This must happen at the very end ***/
            GraphicsHandler.Frame(Camera);

            DirectInput di = new DirectInput();

            /* Create Mouse Handler */
            MouseHandler = new MouseHandler(di);

            /* Create the Keyboard Handler */
            KeyboardHandler = new KeyboardHandler(di);

            /* Set cursor in the middle start of the frame. */
            Cursor.Position = renderForm.PointToScreen(new Point(renderForm.ClientSize.Width / 2, renderForm.ClientSize.Height / 2));
            p = Cursor.Position;
        }

        /// <summary>
        /// Sets the seletable model string names.
        /// </summary>
        private void SetUpSpawnOptions()
        {
            selectableModels.Add(Key.D1, "Raccoon");
            selectableModels.Add(Key.D2, "Basketball");
            selectableModels.Add(Key.D3, "Sphere");
            selectableModels.Add(Key.D4, "Block");
            selectableModels.Add(Key.D5, "Tennisball");
            selectableModels.Add(Key.D6, "Brick Wall");
            selectableModels.Add(Key.D7, "");
            selectableModels.Add(Key.D8, "");
            selectableModels.Add(Key.D9, "");
            selectableModels.Add(Key.D0, "");
        }

        /// <summary>
        /// Handles all the inputs.
        /// </summary>
        private void HandleInputs(float time)
        {
            /* Update DirectInput States */
            MouseHandler.Update();
            KeyboardHandler.Update();

            /* Mouse Click Handling */
            MouseButtonHandling();

            //Handle mouse movement
            MouseMovementHandling((float)(px.X - p.X) * time / 50, (float)(px.Y - p.Y) * time / 50, 0);

            KeyboardMovementHandling(time);

            KeyboardControlHandling(time);

            UIHandling();

            UpdateHeldObject();


            //If the user holding an obj, it will pause the game immediately.
            if (heldObject != null && cameraMode)
            {
                PauseMode = true;
                pauseButton.on();
            }
            if (heldObject != null)
            {
                PauseMode = true;
                pauseButton.on();
            }
            if (heldObject != null)
            {
                DisplayParametersPanel();
            }
            else
            {
                parameterPanel.Hide();
            }

        }

        /// <summary>
        /// Handles mouse button events. 
        /// </summary>
        public void MouseButtonHandling()
        {
            if (MouseHandler.GetKeyDown(MouseKeys.RButton))
            {
                if (heldObject == null)
                {
                    heldObject = ObjectPicker.PickingObjects(renderForm, Camera, renderForm.Width, renderForm.Height, GraphicsHandler);
                    VectorPainter.HeldObject = heldObject;

                    /* If picked something, stop it from being affected by physics */
                    if (heldObject != null)
                    {
                        if (!(heldObject.PhysicsObject is Block))
                            heldObject.Fixed = false;
                        PauseMode = true;
                        pauseButton.on();
                    }
                    else
                    {
                        UIMode = false;
                    }
                }
            }

            /* If left mouse button is pressed. */
            if (MouseHandler.GetKeyDown(MouseKeys.LButton))
            {
                /* Attempt to pick */
                GameObject pickedObject = ObjectPicker.PickingObjects(renderForm, Camera, renderForm.ClientSize.Width, renderForm.ClientSize.Height, GraphicsHandler);

                /* If picked something, stop it from being affected by physics */
                if (pickedObject != null)
                {
                        
                    PauseMode = true;
                    pauseButton.on();
                }
                else
                {
                    UIMode = false;

                }

                /* If clicked on previous object and did not click on any gui elements */
                if (heldObject != null && heldObject == pickedObject && !MouseHandler.GetKeyDown(MouseKeys.RButton)
                    && !GUIRenderer.HandleInputs(new Vector2(renderForm.PointToClient(Cursor.Position).X, renderForm.PointToClient(Cursor.Position).Y)))
                {
                    throwTimer = Stopwatch.StartNew();
                    ApplyChanges();
                    foreach (GUIObject obj in parameterPanel.GuiObjects)
                    {
                        obj.UpdateTexture();
                    }

                    foreach (GUIObject obj in settingsPanel.GuiObjects)
                    {
                        if (obj is TextBox)
                            obj.UpdateTexture();
                    }

                    selectedGUI = null;
                    UIMode = false;
                }

                /* If an object has been picked, store it */
                if (pickedObject != null)
                {
                    heldObject = pickedObject;
                }
                VectorPainter.HeldObject = heldObject;

                /* If an object is held but pressed on GUI elements */
                if (heldObject != null && GUIRenderer.HandleInputs(new Vector2(renderForm.PointToClient(Cursor.Position).X, renderForm.PointToClient(Cursor.Position).Y)))
                {
                    PauseMode = true;
                    pauseButton.on();
                    UIMode = true;
                    selectedGUI = GUIRenderer.selectedPanel.selectedGUI;
               
                }
                /* If presssed on gui elements */
                else if( GUIRenderer.HandleInputs(new Vector2(renderForm.PointToClient(Cursor.Position).X, renderForm.PointToClient(Cursor.Position).Y)))
                {
                    selectedGUI = GUIRenderer.selectedPanel.selectedGUI;
                    if (selectedGUI == gravityXText || selectedGUI == gravityYText || selectedGUI == gravityZText || selectedGUI == dragText || selectedGUI == restitutionText || selectedGUI == randomForceText ||  selectedGUI == rollingFrictionText)
                        UIMode = true;
                    else 
                        UIMode = false;
                }
                else if (pickedObject == null) /* Clicked nothing */
                {
                    if (heldObject != null)
                    {
                        if(heldObject.PhysicsObject is Collidable)
                            (heldObject.PhysicsObject as Collidable).CollisionEnabled = true;
                    }
                    heldObject = null;
                    VectorPainter.HeldObject = null;
                    holdingMode = false;
                }
            }
            /* While left mouse button is down */
            else if (MouseHandler.GetKey(MouseKeys.LButton))
            {
                if (heldObject != null && throwTimer != null)
                {
                    /* Throw if held over minimum threshold */
                    if (throwTimer.ElapsedMilliseconds >= THROW_MIN_TIME)
                    {
                        ThrowVelocity = Camera.LookAt * (Math.Max((throwTimer.ElapsedMilliseconds - THROW_MIN_TIME), 0) * THROW_SPEED);
                    }
                }
            }
            /* User released left button */
            else if (MouseHandler.GetKeyUp(MouseKeys.LButton))
            {
                #region Check selected GUI
                //if user click the apply button, it will apply changes from all textboxes.
                if (selectedGUI == gravitySwitch)
                {

                    if (gravity.Enabled == true)
                    {
                        gravitySwitch.off();
                        gravity.Enabled = false;
                        
                    }
                    else
                    {
                        gravitySwitch.on();
                        gravity.Enabled = true;
                    }
                    
                    UIMode = false;
                    selectedGUI = null;
                } //if drag switch is selected
                else if (selectedGUI == dragSwitch)
                {
                    if (drag.Enabled == true)
                    {
                        dragSwitch.off();
                        drag.Enabled = false;
                    }
                    else
                    {
                        dragSwitch.on();
                        drag.Enabled = true;
                    }

                    UIMode = false;
                    selectedGUI = null;
                } //if randomforcefield switch is selected
                else if(selectedGUI == rollingFrictionSwitch)
                {
                    if (rollingResistance.Enabled == true)
                    {
                        rollingFrictionSwitch.off();
                        rollingResistance.Enabled = false;
                    }
                    else
                    {
                        rollingFrictionSwitch.on();
                        rollingResistance.Enabled = true;
                    }
                }
                else if (selectedGUI == randomForceSwitch)
                {
                    if (randomForceField.Enabled == true)
                    {
                        randomForceSwitch.off();
                        randomForceField.Enabled = false;
                    }
                    else
                    {
                        randomForceSwitch.on();
                        randomForceField.Enabled = true;
                    }
                       
                    UIMode = false;
                    selectedGUI = null;
                } //if settings button is selected.
                else if (selectedGUI == settingsButt)
                {
                    if (settingsPanel.isShow)
                        settingsPanel.Hide();
                    else
                        settingsPanel.Show();

                    selectedGUI = null;
                } //if pause button is selected
                else if (selectedGUI == pauseButton)
                {         
                    if (PauseMode == false)
                    {
                        PauseMode = true;
                        pauseButton.on();            
                    }
                    else
                    {
                        PauseMode = false;
                        pauseButton.off();         
                    }
                    selectedGUI = null;
                }
                else if (selectedGUI == tennisInv)
                {
                    PauseMode = true;
                    pauseButton.on();
                    SpawnSelectedObject(Key.D5);
                    UIMode = false;
                    selectedGUI = null;

                }//spawning sphere  
                else if (selectedGUI == sphereInv)
                {
                    PauseMode = true;
                    pauseButton.on();
                    SpawnSelectedObject(Key.D3);
                    UIMode = false;
                    selectedGUI = null;

                }  // spawning block
                else if (selectedGUI == blockInv)
                {
                    PauseMode = true;
                    pauseButton.on();
                    SpawnSelectedObject(Key.D4);
                    UIMode = false;
                    selectedGUI = null;
                } //spawning raccoon
                else if (selectedGUI == raccoonInv)
                {
                    PauseMode = true;
                    pauseButton.on();
                    SpawnSelectedObject(Key.D1);
                    UIMode = false;
                    selectedGUI = null;
                } //spawning car
                else if (selectedGUI == carInv)
                {
                    PauseMode = true;
                    pauseButton.on();
                    SpawnSelectedObject(Key.D2);
                    UIMode = false;
                    selectedGUI = null;
                }
                else if (selectedGUI == brickInv)
                {
                    PauseMode = true;
                    pauseButton.on();
                    SpawnSelectedObject(Key.D6);
                    UIMode = false;
                    selectedGUI = null;
                }
                else if (selectedGUI == centForceSwitch)
                {
                   
                    if (centForceSwitch.isOn)
                    {
                        centForceSwitch.off();
                        heldObject.PhysicsObject.ForceField.Enabled = false;
                        selectedGUI = null;
                        UIMode = false;
                    }
                    else
                    {
                        centForceSwitch.on();

                        if (heldObject.PhysicsObject.ForceField == null)
                        {
                            CentricForceField cf = new CentricForceField(heldObject.PhysicsObject, Convert.ToDouble(centForceText.Text), Attenuation.QUADRATIC);
                            PhysicsEngine.AddForceField(cf);
                        }

                        heldObject.PhysicsObject.ForceField.Enabled = true;
                        selectedGUI = null;
                        UIMode = false;
                    }
                }
                else if (selectedGUI == resetButton)
                {
                    if(heldObject.Fixed == false)
                         heldObject.Velocity = new Vector3(0.0f, 0.0f, 0.0f);

                    resetButton.UpdateTexture();
                    selectedGUI = null;
                    UIMode = false;
                }
                else if (selectedGUI == deleteButton)
                {
                    if (heldObject != null)
                    {
                        GameObject.Destroy(heldObject);
                        heldObject = null;
                        VectorPainter.HeldObject = null;
                    }

                    deleteButton.UpdateTexture();
                    selectedGUI = null;
                    UIMode = false;
                }
                else if (selectedGUI == fixButton)
                {
                    
                    heldObject.Fixed = true;
                    fixButton.UpdateTexture();
                    resetButton.Hide();
                    selectedGUI = null;

                    UIMode = false;
                }
                #endregion

                /* If holding an object and throwing timer has been activated */
                if (heldObject != null && throwTimer != null)
                {
                    
                    /* Allow the object to be affected by gravity 
                    if (!(heldObject.PhysicsObject is Block))
                    {
                        heldObject.Fixed = false;
                    }
                    */
                    Camera.LookAt.Normalize();

                    /* Throw if held over minimum threshold and object held is sphere */
                    if (throwTimer.ElapsedMilliseconds >= THROW_MIN_TIME && !heldObject.Fixed && heldObject.PhysicsObject is Sphere)
                    {
                        heldObject.Velocity = ThrowVelocity;
                        ThrowVelocity = Vector3.Zero;
                    }

                    if (heldObject.PhysicsObject is Collidable)
                    {
                        (heldObject.PhysicsObject as Collidable).CollisionEnabled = true;
                    }

                    throwTimer.Stop();
                    throwTimer = null;
                    SelectedModel = null;
                    heldObject = null;
                    VectorPainter.HeldObject = null;
                    holdingMode = false;
                }
            }

            /* If right mouse button is lifted */
            if (MouseHandler.GetKeyUp(MouseKeys.RButton))
            {
                /* If holding an object */
                if (heldObject != null)
                {
                    holdingMode = !holdingMode;
                }
            }
        }

        /// <summary>
        /// Mouse movement handler for camera handling.
        /// </summary>
        /// <param name="deltaX">The difference in screen coordinate on the X axis.</param>
        /// <param name="deltaY">The difference in screen coordinate on the Y axis.</param>
        /// <param name="deltaZ">The difference in screen coordinate on the Z axis (Mouse wheel).</param>
        public void MouseMovementHandling(float deltaX, float deltaY, float deltaZ)
        {
            /* Mouse movement handling */
            if (cameraMode)
            {
                //get next point
                px = Cursor.Position;
                Camera.Yaw(deltaX);
                if (deltaY != 0)
                {
                    Camera.Pitch(deltaY);
                }

                //Set the moved cursor as the middle
                Cursor.Position = renderForm.PointToScreen(new Point(renderForm.ClientSize.Width / 2, renderForm.ClientSize.Height / 2));
            }

            /* If holding an object */
            if (heldObject != null)
            {
                double scaleFactor = 1 + ((float)MouseHandler.GetMouseScroll() / 500.0f);
                double maxComponent;
                
                PhysicsObject obj = heldObject.PhysicsObject;

                //Scaling Check
                maxComponent = Math.Max(Math.Max(obj.Scaling.X, obj.Scaling.Y), obj.Scaling.Z);

                if(maxComponent * scaleFactor > 4.0)
                {
                    scaleFactor = 4.0 / maxComponent;
                }
                else if(maxComponent * scaleFactor < 0.01)
                {
                    scaleFactor = 0.01 / maxComponent;
                }

                obj.Scale(scaleFactor);

            }
        }

        /// <summary>
        /// Handle movement related keyboard controls using DirectInput system.
        /// </summary>
        /// <param name="time">The amount of time in millisecond that has passed.</param>
        public void KeyboardMovementHandling(float time)
        {
            /* Find the movement speed based on whether the player is running or not */
            float movementSpeed;
            if (KeyboardHandler.GetKey(Key.LeftShift))
            {
                movementSpeed = RUN_SPEED;
            }
            else
            {
                movementSpeed = WALK_SPEED;
            }

            if (KeyboardHandler.GetKey(Key.W))
            {
                Camera.Walk(movementSpeed * time);
            }
            if (KeyboardHandler.GetKey(Key.S))
            {
                Camera.Walk(-movementSpeed * time);
            }
            if (KeyboardHandler.GetKey(Key.D))
            {
                Camera.Strafe(movementSpeed * time);
            }
            if (KeyboardHandler.GetKey(Key.A))
            {
                Camera.Strafe(-movementSpeed * time);
            }
            if (KeyboardHandler.GetKey(Key.Space))
            {
                Camera.Fly(movementSpeed * time);
            }
            if (KeyboardHandler.GetKey(Key.LeftControl))
            {
                Camera.Fly(-movementSpeed * time);
            }
            if (KeyboardHandler.GetKey(Key.Left))
            {
                Camera.Yaw(-10);
            }
            if (KeyboardHandler.GetKey(Key.Right))
            {
                Camera.Yaw(10);
            }
            if (KeyboardHandler.GetKey(Key.Up))
            {
                Camera.Pitch(10);
            }
            if (KeyboardHandler.GetKey(Key.Down))
            {
                Camera.Pitch(-10);
            }
        }

        /// <summary>
        /// Handle non-movement related keyboard controls using the DirectInput system.
        /// </summary>
        /// <param name="time">The amount of time in millisecond that has passed.</param>
        private void KeyboardControlHandling(float time)
        {
            if (UIMode == false)
                ObjectSpawningControlHandling();

            if (KeyboardHandler.GetKeyDown(Key.C))
            {
                if (cameraMode)
                {
                    cameraMode = false;
                    Cursor.Show();
                    Cursor.Clip = Rectangle.Empty;
                    csPanel.Hide();
                }
                else if (!cameraMode)
                {
                    cameraMode = true;

                    /* Reduce jerking motion upon activation */
                    //Set the moved cursor as the middle and grab position
                    Cursor.Position = renderForm.PointToScreen(new Point(renderForm.ClientSize.Width / 2, renderForm.ClientSize.Height / 2));
                    p = Cursor.Position;
                    px = Cursor.Position;

                    //Hide cursor
                    Cursor.Hide();
                    //Clip cursor just for the active window
                    Cursor.Clip = new Rectangle(renderForm.Location, renderForm.Size);
                    csPanel.Show();

                }
            }
            //Enbaling and disabling gravity
            if (KeyboardHandler.GetKeyDown(Key.G))
            {
               
                if (gravity.Enabled == true)
                {
                    gravitySwitch.off();
                    gravity.Enabled = false;
                    selectedGUI = null;
                }
                else
                {
                    gravitySwitch.on();
                    gravity.Enabled = true;
                    selectedGUI = null;
                }
            }
            //Enablicng and disabling random force field
            if (KeyboardHandler.GetKeyDown(Key.R))
            {  
                if (randomForceField.Enabled == true)
                {
                    randomForceSwitch.off();
                    randomForceField.Enabled = false;
                    selectedGUI = null;
                }
                else
                {
                    randomForceSwitch.on();
                    randomForceField.Enabled = true;
                    selectedGUI = null;
                }
            }
            //Enabling pause mode
            if (KeyboardHandler.GetKeyDown(Key.P))
            {
                if (PauseMode == false)
                {
                    PauseMode = true;
                    pauseButton.on();

                }
                else if (PauseMode == true)
                {
                    
                    if (heldObject != null && heldObject.PhysicsObject is Collidable)
                    {
                        (heldObject.PhysicsObject as Collidable).CollisionEnabled = true;
                    }

                    heldObject = null;
                    
                    VectorPainter.HeldObject = null;

                    PauseMode = false;
                    pauseButton.off();
                }
            }
            //Enabling and disablig drag
            if (KeyboardHandler.GetKeyDown(Key.I))
            {
                if (drag.Enabled == false)
                {
                    drag.Enabled = true;
                    dragSwitch.on();
                }
                else if (drag.Enabled == true)
                {
                    drag.Enabled = false;
                    dragSwitch.off();
                }
            }
            /*** This MUST be last ***/
            if (KeyboardHandler.GetKeyDown(Key.Escape))
            {
                renderForm.Close();
            }
            /* Kill object */
            if (KeyboardHandler.GetKeyDown(Key.Delete))
            {
                if (heldObject != null)
                {
                    GameObject.Destroy(heldObject);
                    heldObject = null;
                    VectorPainter.HeldObject = null;
                }
            }
        }

        /// <summary>
        /// Keyboard handling for the UI. this is for the numbers only
        /// </summary>
        private void UIHandling()
        {
            //If UI mode is true
            if (UIMode)
            {
                if (selectedGUI != null) {

                    //save the current text
                    string currText;
                    string period = ".";

                    //if user press enter it will apply the change happen in the selected box.
                    if (KeyboardHandler.GetKeyDown(Key.Return) || KeyboardHandler.GetKeyDown(Key.NumberPadEnter))
                    {
                        
                        ApplyChanges();
                        foreach (GUIObject obj in parameterPanel.GuiObjects)
                        {
                            obj.UpdateTexture();
                        }

                        foreach (GUIObject obj in settingsPanel.GuiObjects)
                        {
                            if(obj is TextBox)
                                obj.UpdateTexture();
                        }
                        
                        selectedGUI = null;
                        UIMode = false;
                    }

                    //this  following line will use to handle the text box editing.
                    if (KeyboardHandler.GetKeyDown(Key.Period) || KeyboardHandler.GetKeyDown(Key.NumberPadComma))
                    {
                        currText = selectedGUI.Text;
                        selectedGUI.Text = currText + period;
                    }

                    if (KeyboardHandler.GetKeyDown(Key.Subtract))
                    {
                        currText = selectedGUI.Text;
                        selectedGUI.Text = currText + "-";
                    }
                    if (KeyboardHandler.GetKeyDown(Key.Back))
                    {
                        currText = selectedGUI.Text;
                        if (currText.Length != 0)
                        {
                            selectedGUI.Text = currText.Substring(0, currText.Length - 1);
                        }
                    }
                    else
                    {
                        //FOR number only
                        Key[] pressed = { Key.D0, Key.D1, Key.D2, Key.D3, Key.D4, Key.D5, Key.D6, Key.D7, Key.D8, Key.D9, 
                                            Key.NumberPad0, Key.NumberPad1, Key.NumberPad2, Key.NumberPad3, Key.NumberPad4, Key.NumberPad5, Key.NumberPad6, Key.NumberPad7, Key.NumberPad8, Key.NumberPad9, 
                                            Key.Minus,};

                        foreach (Key k in pressed)
                        {
                            if (KeyboardHandler.GetKeyDown(k))
                            {
                                currText = selectedGUI.Text;

                                selectedGUI.Text = currText + InputTextDictionary[k];
                                break;

                            }
                        }
                    }
                }
            }  
        }

        /// <summary>
        /// Apply changes throughout the GUI component.
        /// </summary>
        private void ApplyChanges()
        {
            if (heldObject != null)
            {

                if (heldObject.PhysicsObject is Sphere)
                {
                    if (selectedGUI == densityText)
                        ((Collidable)(heldObject.PhysicsObject)).Density = Convert.ToDouble(densityText.Text);
                    else if (selectedGUI == posXText)       
                        heldObject.PhysicsObject.Position.X = Convert.ToDouble(posXText.Text);
                    else if(selectedGUI == posYText)
                        heldObject.PhysicsObject.Position.Y = Convert.ToDouble(posYText.Text);
                    else if(selectedGUI == posZText)
                        heldObject.PhysicsObject.Position.Z = Convert.ToDouble(posZText.Text);
                    else if(selectedGUI == massText)
                        heldObject.PhysicsObject.Mass = Convert.ToDouble(massText.Text);
                    else if(selectedGUI == scaleXText)
                        heldObject.PhysicsObject.SetScaling(Convert.ToDouble(scaleXText.Text));
                    else if(selectedGUI == rotXText)
                        heldObject.PhysicsObject.Rotation.X = Convert.ToDouble(rotXText.Text) * Math.PI / 180;
                    else if(selectedGUI == rotYText)
                        heldObject.PhysicsObject.Rotation.Y = Convert.ToDouble(rotYText.Text) * Math.PI / 180;
                    else if(selectedGUI == rotZText)
                        heldObject.PhysicsObject.Rotation.Z = Convert.ToDouble(rotZText.Text) * Math.PI / 180;
                    else if (selectedGUI == centForceText)
                    {
                        /* If this object has a forcefield, update the acceleration */
                        if (heldObject.PhysicsObject.ForceField != null)
                        {
                            heldObject.PhysicsObject.ForceField.Acceleration = Convert.ToDouble(centForceText.Text);
                        }
                    }else if(selectedGUI == velXText)
                        heldObject.PhysicsObject.Velocity.X = Convert.ToDouble(velXText.Text);
                    else if(selectedGUI == velYText)
                        heldObject.PhysicsObject.Velocity.Y = Convert.ToDouble(velYText.Text);
                    else if(selectedGUI == velZText)
                        heldObject.PhysicsObject.Velocity.Z = Convert.ToDouble(velZText.Text);
                }
                else
                {
                    if(selectedGUI == massText)
                        heldObject.PhysicsObject.Mass = Convert.ToDouble(massText.Text);
                    else if(selectedGUI == scaleXText || selectedGUI == scaleYText || selectedGUI == scaleZText)
                        heldObject.PhysicsObject.SetScaling(
                            Convert.ToDouble(scaleXText.Text),
                            Convert.ToDouble(scaleYText.Text),
                            Convert.ToDouble(scaleZText.Text));
                    else if(selectedGUI == rotXText)
                        heldObject.PhysicsObject.Rotation.X = Convert.ToDouble(rotXText.Text) * Math.PI / 180;
                    else if(selectedGUI == rotYText)
                        heldObject.PhysicsObject.Rotation.Y = Convert.ToDouble(rotYText.Text) * Math.PI / 180;
                    else if(selectedGUI == rotZText)
                        heldObject.PhysicsObject.Rotation.Z = Convert.ToDouble(rotZText.Text) * Math.PI / 180;
                    else if(selectedGUI == centForceText)
                        /* If this object has a forcefield, update the acceleration */
                        if (heldObject.PhysicsObject.ForceField != null)
                        {
                            heldObject.PhysicsObject.ForceField.Acceleration = Convert.ToDouble(centForceText.Text);
                        }
                } 
            } 
            else if(selectedGUI == gravityXText)
            {
                    gravity.Acceleration.X = Convert.ToDouble(gravityXText.Text);
                    selectedGUI.UpdateTexture();
                    selectedGUI = null;
            }
            else if (selectedGUI == gravityYText)
            {
                gravity.Acceleration.Y = Convert.ToDouble(gravityYText.Text);
                selectedGUI.UpdateTexture();
                selectedGUI = null;
            }
            else if (selectedGUI == gravityZText)
            {
                gravity.Acceleration.Z = Convert.ToDouble(gravityZText.Text);
                selectedGUI.UpdateTexture();
                selectedGUI = null;
            }
            else if (selectedGUI == dragText)
            {
                drag.Density = Convert.ToDouble(dragText.Text);
                selectedGUI.UpdateTexture();
                selectedGUI = null;
            }
            else if (selectedGUI == restitutionText)
            {
                PhysicsEngine.RestitutionCoefficient = Convert.ToDouble(restitutionText.Text);
                selectedGUI.UpdateTexture();
                selectedGUI = null;
            }
            else if (selectedGUI == randomForceText)
            {
                randomForceField.Acceleration  = Convert.ToDouble(randomForceText.Text);
                selectedGUI.UpdateTexture();
                selectedGUI = null;
            }
            else if (selectedGUI == rollingFrictionText)
            {
                rollingResistance.RollingCoefficient = Convert.ToDouble(rollingFrictionText.Text);
                selectedGUI.UpdateTexture();
                selectedGUI = null;
            }

        }

        /// <summary>
        /// Handles the controls for selecting which game objects to spawn.
        /// </summary>
        private void ObjectSpawningControlHandling()
        {
            foreach (Key key in spawnKeys)
            {
                if (KeyboardHandler.GetKeyDown(key))
                {
                    SpawnSelectedObject(key);
                }
            }
        }

        private void SpawnSelectedObject(Key key)
        {
            /* Allow the old held object to be affected by gravity */
            if (heldObject != null)
            {
                //Drop current holding object
                if (heldObject.PhysicsObject is Block)
                {

                }
                else
                {
                    heldObject.Fixed = false;
                }

                if (heldObject.PhysicsObject is Collidable)
                {
                    (heldObject.PhysicsObject as Collidable).CollisionEnabled = true;
                }
                
                heldObject = null;
                VectorPainter.HeldObject = null;
            }

            /* Create the selected object */
            SelectedModel = selectableModels[key];
            heldObject = GameObject.CreateByName(SelectedModel, Camera.GetPositionInFront(DISTANCE_TO_SPAWN), false);
            
            /* Check if an object has been spawned */
            if (heldObject != null)
            {
                VectorPainter.HeldObject = heldObject;

                if (heldObject.PhysicsObject is Collidable)
                {
                    (heldObject.PhysicsObject as Collidable).CollisionEnabled = false;
                }


                holdingMode = true;
                PauseMode = true;

                if (heldObject.PhysicsObject is Block)
                {

                }
                else
                {
                    heldObject.Fixed = false;
                }
            }
        }

        /// <summary>
        /// Update the position the player is currently holding.
        /// </summary>
        private void UpdateHeldObject()
        {
            if (heldObject != null && holdingMode)
            {
                heldObject.Position = Camera.GetPositionInFront(DISTANCE_TO_SPAWN + 
                    Math.Max(Math.Max((float)heldObject.PhysicsObject.Scaling.X, (float)heldObject.PhysicsObject.Scaling.Y), (float)heldObject.PhysicsObject.Scaling.Z));
            }
        }

        /// <summary>Returns true if the current application has focus, false otherwise</summary>
        public static bool ApplicationIsActivated()
        {
            var activatedHandle = GetForegroundWindow();
            if (activatedHandle == IntPtr.Zero)
            {
                return false;       // No window is currently activated
            }

            var procId = Process.GetCurrentProcess().Id;
            int activeProcId;
            GetWindowThreadProcessId(activatedHandle, out activeProcId);

            return activeProcId == procId;
        }

        private void DisplayParametersPanel()
        {
            if (heldObject.PhysicsObject is Sphere)
            {
                parameterPanel.Show();
                rotXLabel.Hide();
                rotYLabel.Hide();
                rotZLabel.Hide();
                rotXText.Hide();
                rotYText.Hide();
                rotZText.Hide();
                scaleLabel.Hide();
                scaleYText.Hide();
                scaleZText.Hide();
                scaleXLabel.Hide();
                scaleYLabel.Hide();
                scaleZLabel.Hide();
                velNA.Hide();
                velMagNA.Hide();
            }
            else
            {
                parameterPanel.Show();
                radiusLabel.Hide();
                rotNA.Hide();

                /* Hide all velocity stuff */
                velLabelX.Hide();
                velLabelY.Hide();
                velLabelZ.Hide();
                velXText.Hide();
                velYText.Hide();
                velZText.Hide();
                velMag.Hide();
            }

            if (heldObject.PhysicsObject is Collidable)
            {
                densityNA.Hide();
                centForceNA.Hide();
            }
            else
            {
                densityText.Hide();
                centForceText.Hide();
                centForceSwitch.Hide();
                
            }
        }

        [DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
        private static extern IntPtr GetForegroundWindow();

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern int GetWindowThreadProcessId(IntPtr handle, out int processId);

        #endregion
    }
}
