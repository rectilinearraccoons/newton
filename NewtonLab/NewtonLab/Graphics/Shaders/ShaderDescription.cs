﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SharpDX.Direct3D11;

namespace NewtonLab.Graphics.Shaders
{
    class ShaderDescription
    {
        /// <summary>
        /// Vertex Shader Function Name
        /// </summary>
        public string VertexShaderFunction { get; set; }

        /// <summary>
        /// Pixel Shader Function Name
        /// </summary>
        public string PixelShaderFunction { get; set; }

        /// <summary>
        /// Geometry Shader Function Name
        /// </summary>
        public string GeometryShaderFunction { get; set; }

        /// <summary>
        /// Hull Shader Function Name
        /// </summary>
        public string HullShaderFunction { get; set; }

        /// <summary>
        /// Domain Shader Function Name
        /// </summary>
        public string DomainShaderFunction { get; set; }

        /// <summary>
        /// Stream output elements
        /// </summary>
        public StreamOutputElement[] GeometrySO { get; set; }
    }
}
