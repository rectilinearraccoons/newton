﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SharpDX;

namespace NewtonLab.Graphics.ObjImport
{
    struct StaticVertex
    {       
        /// <summary>
        /// Position
        /// </summary>
        public Vector3 Position;

        /// <summary>
        /// Normal
        /// </summary>
        public Vector3 Normal;

        /// <summary>
        /// Texture coordinate
        /// </summary>
        public Vector2 TextureCoordinate;

        /// <summary>
        /// Compare 2 vertices on Position and Texture Coordinate
        /// </summary>
        /// <param name="a">First vertex</param>
        /// <param name="b">Second vertex</param>
        /// <returns>Returns true if the vertices are the same.</returns>
        internal static bool Compare(StaticVertex a, StaticVertex b)
        {
            return a.Position == b.Position &&
                a.TextureCoordinate == b.TextureCoordinate;
        }


        
    }
}
